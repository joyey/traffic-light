library lights;

enum Colors { RED, YELLOW, GREEN }
enum Power { OFF, ON }

class LightCommand {
  final Colors color;
  final Power power;

  const LightCommand(this.color, this.power);

  LightCommand.RedOff() : this(Colors.RED, Power.OFF);
  LightCommand.RedOn() : this(Colors.RED, Power.ON);
  LightCommand.YellowOff() : this(Colors.YELLOW, Power.OFF);
  LightCommand.YellowOn() : this(Colors.YELLOW, Power.ON);
  LightCommand.GreenOff() : this(Colors.GREEN, Power.OFF);
  LightCommand.GreenOn() : this(Colors.GREEN, Power.ON);

  @override
  String toString() {
    return color.toString() + ':' + power.toString();
  }
}

abstract class _TrafficLightState {
  late final _TrafficLightState nextState = _RedState();
  Stream<LightCommand> produce();
}

class _RedState implements _TrafficLightState {
  @override
  late final _TrafficLightState nextState = _TurningGreenState();

  @override
  Stream<LightCommand> produce() async* {
    yield LightCommand.YellowOff();
    yield LightCommand.RedOn();
    await Future.delayed(const Duration(seconds: 5));
  }
}

class _TurningGreenState implements _TrafficLightState {
  @override
  late final _TrafficLightState nextState = _GreenState();

  @override
  Stream<LightCommand> produce() async* {
    yield LightCommand.YellowOn();
    await Future.delayed(const Duration(seconds: 3));
    yield LightCommand.RedOff();
    await Future.delayed(const Duration(seconds: 2));
  }
}

class _GreenState implements _TrafficLightState {
  @override
  late final _TrafficLightState nextState = _TurningRedState();

  @override
  Stream<LightCommand> produce() async* {
    yield LightCommand.GreenOn();
    await Future.delayed(const Duration(seconds: 2));
    yield LightCommand.YellowOff();
    await Future.delayed(const Duration(seconds: 5));
  }
}

class _TurningRedState implements _TrafficLightState {
  @override
  late final _TrafficLightState nextState = _RedState();

  @override
  Stream<LightCommand> produce() async* {
    yield LightCommand.GreenOff();
    yield LightCommand.YellowOn();
    await Future.delayed(const Duration(seconds: 5));
  }
}

class TrafficLights {
  _TrafficLightState currentState = _RedState();

  Stream<LightCommand> produce() async* {
    while(true) {
      await for (final event in currentState.produce()) {
        yield event;
      }
      currentState = currentState.nextState;
    }
  }
}

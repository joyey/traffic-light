import 'dart:async';

import 'package:flutter/material.dart';
import 'lights.dart' as lights;

void main() {
  const title = 'Liikennevaloissa';
  return runApp(MaterialApp(
    home: Scaffold(
        appBar: AppBar(title: Text(title)),
        body: const Center(child: TrafficLightWidget()),
        )),
  );
}

class TrafficLightWidget extends StatefulWidget {
  const TrafficLightWidget({Key? key}) : super(key: key);

  @override
  _TrafficLightWidgetState createState() => _TrafficLightWidgetState();
}

class _TrafficLightWidgetState extends State<TrafficLightWidget> {
  bool _isRedOn = false;
  bool _isYellowOn = false;
  bool _isGreenOn = false;

  final lights.TrafficLights lightController = lights.TrafficLights();
  late final StreamSubscription subscription;

  @override
  void initState() {
    super.initState();
    subscription = lightController.produce().listen((command) {
      switch (command.color) {
        case lights.Colors.RED:
          setState(() {
            _isRedOn = command.power == lights.Power.ON;
          });
          break;
        case lights.Colors.YELLOW:
          setState(() {
            _isYellowOn = command.power == lights.Power.ON;
          });
          break;
        case lights.Colors.GREEN:
          setState(() {
            _isGreenOn = command.power == lights.Power.ON;
          });
          break;
      }
    });
  }

  @override
  void dispose() {
    subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey,
      child: Column(children: [
        Expanded(
          child: FittedBox(
              child: Icon(Icons.circle,
                  color: (_isRedOn ? Colors.red : Colors.black)),
              fit: BoxFit.fill),
        ),
        Expanded(
            child: FittedBox(
                child: Icon(Icons.circle,
                    color: (_isYellowOn ? Colors.yellow : Colors.black)),
                fit: BoxFit.fill)),
        Expanded(
            child: FittedBox(
                child: Icon(Icons.circle,
                    color: (_isGreenOn ? Colors.green : Colors.black)),
                fit: BoxFit.fill))
      ]),
    );
  }
}

# Traffic light

A simple exercise in Dart & Flutter for the Pragmatic Programmer study group

## Concept

The application contains a state machine where different states are implemented
as classes that produce commands for traffic lights to turn on or off in an asynchronous
manner to a Stream. The stream is consumed by an UI widget that renders based on the commands.